<?php

session_start();

function get_database() {
    $database = array_key_exists('database', $_COOKIE) ? $_COOKIE['database'] : 'mysql';
    switch ($database) {
        case 'mysql':
            return 'mysql';
        case 'mssql':
            return 'mssql';
        default:
            return 'mysql';
    }
}
include_once 'include-' . get_database() . '.php';

// Change database
if (array_key_exists('database', $_REQUEST)) setcookie('database', $_REQUEST['database']);

// Change Debug Flag
if (array_key_exists('debug', $_GET)) setcookie('debug', $_GET['debug']);