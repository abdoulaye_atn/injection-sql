# Description
Application SQL vulnérable

# Prérequis
- docker
- docker-compose

# Installation
```bash
# Télécharger le repo
git clone https://gitlab.com/sebast331-ctf/injection-sql
cd injection-sql

# Lancer l'application
docker-compose up
```

# Applications
L'application web est disponible à l'adresse http://localhost:8000  
**phpMyAdmin** est disponible à l'adresse http://localhost:8080

## Compte phpMyAdmin
Nom d'usager: `root`  
Mot de passe: `Password1`
